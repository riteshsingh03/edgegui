import React from 'react';
import { Empty } from 'antd';

const notFound = (props: any) => (
    <div>
        <br/><br/> <Empty/> <br/><br/>
    </div>
);

export default notFound;
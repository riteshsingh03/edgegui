import React from 'react';
import { NavLink } from 'react-router-dom';
import Icon from './Icons';

const menuItem = (props: any) => (
    <div className="menu-item pull-left">
        <NavLink to={props.url} activeClassName="active" className="icon" title={props.label}>
            <Icon name={props.icon} />
        </NavLink>
    </div>
);

export default menuItem;

import React from 'react';

import {
    ShoppingCartOutlined,
    UserAddOutlined,
    RiseOutlined,
    BulbOutlined,
    CalendarOutlined,
    TeamOutlined,
    GlobalOutlined,
    FileTextOutlined
} from '@ant-design/icons';

const Icon = (props: any) => {
    switch (props.name) {
        case "ShoppingCartOutlined":
            return <ShoppingCartOutlined />;
        case "UserAddOutlined":
            return <UserAddOutlined />;
        case "RiseOutlined":
            return <RiseOutlined />;
        case "BulbOutlined":
            return <BulbOutlined />
        case "CalendarOutlined":
            return <CalendarOutlined />;
        case "TeamOutlined":
            return <TeamOutlined />;
        case "GlobalOutlined":
            return <GlobalOutlined />;
        case "FileTextOutlined":
            return <FileTextOutlined />;
        default:
            return <FileTextOutlined />;
    }
};

export default Icon;
import React, { Component } from 'react';
import logo from '../../../assets/img/edgelogo.png';
import { Row, Col, Avatar } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { MENU_DATA } from '../../../assets/json/menu';
import MenuItem from './MenuItem/MenuItem';
import '../../../assets/style/header.scss';
import { SettingOutlined, SyncOutlined } from '@ant-design/icons';

class HeaderContent extends Component {
    render() {
        const navLinsk = MENU_DATA.map((link, index)=> <MenuItem key={index} url={link.url} label={link.label} icon={link.icon} />);

        return (
            <Row className="header-content">
                <Col span={2}>
                    <div className="org-logo">
                        <img src={logo} alt="AT&T Logo" />
                    </div>
                </Col>
                <Col span={16}>
                    <div className="menu">
                        {navLinsk}
                    </div>
                </Col>
                <Col span={6} className="icons-container">
                    
                    <div className="user-icon pull-right">                        
                        <Avatar size={30} icon={<UserOutlined />} />
                    </div>
                    <div className="user-info pull-right">
                        <span className="wl-text">Welcome</span>
                        <span className="un-text">RS727V</span>
                    </div>
                    <div className="icon pull-right">                        
                        <SettingOutlined />
                    </div>
                    <div className="icon pull-right">                        
                        <SyncOutlined />
                    </div>
                </Col>
            </Row>

        )
    }
}

export default HeaderContent;
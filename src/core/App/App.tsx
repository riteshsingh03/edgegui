import React, { Component } from 'react';
import style from './App.module.scss';
import { Layout } from 'antd';
import { BrowserRouter } from 'react-router-dom';
import HeaderContent from '../../shared/components/Header/HeaderContent';
import AppRoute from './../AppRoute';

class App extends Component {

    render() {
        const { Header, Content } = Layout;

        return (
            <Layout>
                <BrowserRouter>
                    <Header className={style.header}>
                        <HeaderContent />
                    </Header>
                    <Content className={style.content}>
                        <div>
                            <AppRoute/>
                        </div>
                    </Content>
                </BrowserRouter>
            </Layout>
        )
    }
}

export default App;

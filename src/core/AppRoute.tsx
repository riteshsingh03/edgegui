import React, { Component, lazy, Suspense } from 'react';
import { Switch, Route, Router } from 'react-router-dom';
//import { createBrowserHistory } from 'history';


const Dispatch = lazy(() => import('./Dispatch/Dispatch'));
const Admin = lazy(() => import('./Admin/Admin'));
const NotFound = lazy(() => import('../shared/components/NotFound/NotFound'));

//const history = createBrowserHistory();

class AppRoute extends Component {
    render() {
        return (
            //<Router history={history}>
                <Suspense fallback={<div>Loading...</div>}>

                    <Switch>
                        <Route path="/dispatch" component={Dispatch}></Route>
                        <Route path="/admin" component={Admin}></Route>
                        <Route path="*" component={NotFound}></Route>
                    </Switch>

                </Suspense>
            //</Router>
        )
    }
}

export default AppRoute;
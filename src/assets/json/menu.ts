export const MENU_DATA = [
    {
        "label": "EDGE Dispatch",
        "icon": "ShoppingCartOutlined",
        "url": "/dispatch",
        "children":[]
    },
    {        
        "label": "EDGE Admin",
        "icon": "UserAddOutlined",
        "url": "admin",
        "children": [
            {
                "label": "User Admin",
                "url": "admin/user"
            },
            {
                "label": "Table Admin",
                "url": "admin/table"
            },
            {
                "label": "FORCE Reorg Admin",
                "url": "admin/reorg"
            }

        ]
    },
    {        
        "label": "Capacity Planning",
        "icon": "RiseOutlined",
        "url": "capacity",
        "children": [
            {
                "label": "Dashboard",
                "url": "capacity/dashboard"
            }, {
                "label": "Lights Page",
                "url": "capacity/lightsPage"
            }, {
                "label": "Daily Plan",
                "url": "capacity/dailyPlan"
            }, {
                "label": "Long Term Plan",
                "url": "capacity/longTerm"
            },
            {
                "label": "Schedule Recommendation",
                "url": "capacity/scheduleRecommendation"
            },
            {
                "label": "Scenario Modeler",
                "url": "capacity/scenarioModeler"
            },
            {
                "label": "Config",
                "url": "capacity/config"
            },
            {
                "label": "Loan Matrix",
                "url": "capacity/ilm"
            },
            {
                "label": "Reports",
                "url": "capacity/reports"
            }
        ]
    },
    {        
        "label": "DLE",
        "icon": "BulbOutlined",
        "url": "dle",
        "children": [
            {
                "label": "Details",
                "url": "dle/details"
            },
            {
                "label": "Config",
                "url": "dle/config"
            }
        ]
    },
    {        
        "label": "Event Rules",
        "icon": "CalendarOutlined",        
        "url": "eventrule",
        "children": [
            {
                "label": "Event Rules",
                "url": "eventrules/eventrule"
            },
            {
                "label": 'Manage Contract',
                "url": 'eventrules/managecontract'
            }
        ]
    },
    {        
        "label": "Resource Management",
        "icon": "TeamOutlined",
        "url": "resource",
        "children": [
            {
                "label": "Resource Management",
                "url": "resource/resourceMngEAM"
            }
        ]
    },
    {        
        "label": "Map",
        "icon": "GlobalOutlined",
        "url":"/map",
        "children": []
    },
    {
        "label": "More",
        "icon": "FileTextOutlined",
        "url": "more",
        "children": [
            {
                "label": "Industry Markets",
                "url": "more/industryMarkets"
            },

            {
                "label": "Region",
                "url": "more/region"
            }, 
            {
                "label": "Web Modeler",
                "url": "more/webModeler"
            },
            {
                "label": "Cockpit",
                "url": "more/cockpit"
            },
            {
                "label": "Reports",
                "url": "more/reports"
            }
        ]
    }
];
